#|/usr/bin/env python3

"Sort a list of songs based on their number of plays."

import sys

def order_items(songs : list, i : int, gap : int) -> list:
    """
    Reorder the 'songs' list based on the 'i' and 'gap' parameters.

    Args:
        songs (list): A list of songs where each item is a tuple (song_name, number_of_plays).
        i (int): The starting index for reordering.
        gap (int): The gap or step size for comparing and moving elements.

    This function reorders the 'songs' list based on the 'i' and 'gap' values, following the Shell Sort algorithm.
    It compares and moves elements within the list to achieve descending order based on the number of plays.

    Returns:
        list: The reordered 'songs' list.
    """
    while i >= gap and songs[i - gap][1] < songs[i][1]:
        songs[i], songs[i - gap] = songs[i - gap], songs[i]
        i -= gap
    return songs


def sort_music(songs: list) -> list:
    """Implement the Shell Sort algorithm to order the dictionary based on the number of plays.

    Args:
        dictionary (dict): A dictionary where keys are song names and values are the number of plays.

    Implement the Shell Sort algorithm to rearrange the songs in the dictionary. Order them in descending
    order based on the number of plays. You should consider songs with more plays as 'higher' in terms of
    popularity and place them towards the beginning of the dictionary.

    Your implementation should update the 'dictionary' in place, resulting in a sorted dictionary.
    """
    n = len(songs)
    gap = n // 2

    while gap > 0:
        for i in range(gap, n):
            order_items(songs, i, gap)
        gap //= 2

    return songs


def create_dictionary(arguments):
    """
    Create a dictionary from a list of arguments.

    Args:
        arguments (list): A list of elements where every pair represents a song name and its number of plays.

    This function takes a list of arguments and converts it into a dictionary where keys are song names, and values are
    the corresponding number of plays.

    Returns:
        dict: A dictionary with song names as keys and the number of plays as values.
    """
    if len(arguments) % 2 != 0 or len(arguments) == 0:
        sys.exit("Error: Debes proporcionar pares de canción y número de reproducciones.")

    songs_dict = {}
    for i in range(0, len(arguments), 2):
        song_name = arguments[i]
        plays = int(arguments[i + 1])
        songs_dict[song_name] = plays

    return songs_dict


def main():
    args = sys.argv[1:]

    songs = create_dictionary(args)
    sorted_songs = sort_music(list(songs.items()))
    sorted_dict = dict(sorted_songs)

    print(sorted_dict)


if __name__ == '__main__':
    main()
